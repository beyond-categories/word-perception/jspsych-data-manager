import pico
import os
import time

# dummy function (hello world equivalent)
@pico.expose()
def hello(who):
    s = "hello %s!" % who
    return s

# function to listen to data and write it to a csv file
@pico.expose()
def save_data(filedata):
    print(filedata)
    timestamp = time.strftime("%Y-%m-%d_%H:%M:%S%z", time.gmtime())
    try:
        with open("data_%s.csv"%str(timestamp), 'w') as outfile:
            outfile.write(filedata)
    except IOError:
        print("Error, could not write to file.")

# # [WIP:TODO] function to update git repo storing data
# def update_repo():
#     pwd = os.getcwd()

app = pico.PicoApp()
app.register_module(__name__)
